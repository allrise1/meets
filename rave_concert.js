let _rave_blex_uuid = '00000000-0000-1000-8000-00805f9b34fb';
var ble = new Array(2);
//var _rave_pair_timer = null;
var _rave_payer = null;
var _rave_ble_idx = 0;
var _rave_sucess_return;

function structDev()
{
	var dev;
	var chr;
}

function isMac()
{
	return /(Mac|iPhone|iPod|iPad)/i.test(navigator.platform);
}

function _rave_youtube_player(player) {
	_rave_payer = player;
}

function _rave_YT_timecheck() {
	//var tm = _rave_payer.getDuration();
	var tm = _rave_payer.getCurrentTime();
	//console.log(tm);
}

function _rave_ble_paring(callback, scallback) {
	//setInterval(_rave_YT_timecheck, 1000);
	if (_rave_ble_idx >= 2) return;
	
	_rave_sucess_return = (callback == undefined)? null:scallback;
	
	let filters = [];

	let options = {};
	filters.push({services: [_rave_blex_uuid]});
	options.filters = filters;

	console.log('Requesting Bluetooth Device...');
	navigator.bluetooth.requestDevice(options)
	.then(device => {
		console.log('> Name:             ' + device.name);
		console.log('> Id:               ' + device.id);
		if(device.gatt.connected) return;
		ble[_rave_ble_idx] = new structDev();
		ble[_rave_ble_idx].dev = device;
		const idx = _rave_ble_idx;
		ble[_rave_ble_idx].dev.addEventListener('gattserverdisconnected', function() { _rave_reconnect(idx); } );
		_rave_connect(false, _rave_ble_idx);
	})
	.catch(error => {
		console.log('Argh! ' + error);
		if (callback != undefined)
			callback(error);
		return;
	});
}

function _rave_bleconnect(idx) {
	//setTimeout(() => _rave_ble_device = null, 5000);
	return ble[idx].dev.gatt.connect();
}

async function _rave_reconnect(idx) {
	if (isMac()) return;
	await _rave_connect(true, idx);
}

function _rave_connect(retry, idx) {
	_rave_bleconnect(idx)
	.then(server => {
		return server.getPrimaryService(_rave_blex_uuid);
	})
	.then(service => {
		return service.getCharacteristic(_rave_blex_uuid);
	})
	.then(characteristic => {
		if (!retry && _rave_sucess_return != null)
			_rave_sucess_return(idx);
		ble[idx].chr = characteristic;
		_rave_ble_idx++;
		var rgb = new Uint8Array([ 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ]);
		return characteristic.writeValue(rgb);
	})
	.catch(error => {
		//console.log('Argh! ' + error);
		if (retry) setTimeout(() => _rave_reconnect(idx), 3000);
		//else     console.log('Argh! ' + error);
	});
}

var loop = true;
function _rave_test() {
	var rgb = new Uint8Array([ 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ]);
	var off = new Uint8Array([ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ]);
    _rave_ble_write(loop?rgb:off);
	loop = !loop;
}

function _rave_ble_write(data) {
	_rave_ble_service.writeValue(data)
	.then(_ => {
		console.log(result);
	})
	.catch(error => {
		console.log(error);
	});
}